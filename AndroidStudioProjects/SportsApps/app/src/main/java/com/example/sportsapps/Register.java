package com.example.sportsapps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity {
    private EditText name, email, pass;
    private Button register;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private final String ARCHIVO = "Login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        name = findViewById(R.id.nombre);
        email = findViewById(R.id.correo);
        pass = findViewById(R.id.password);
        register = findViewById(R.id.register);

        prefs = getSharedPreferences(ARCHIVO, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void registrar(View v){
        String nombre = name.getText().toString();
        String correoE = email.getText().toString();
        String contra = pass.getText().toString();

        if(correoE.contains("@")){

            editor.putString("name", nombre);
            editor.putString("email", correoE);
            editor.putString("password", contra);
            editor.commit();

            Toast.makeText(this, "REGISTRADO CORRECTAMENTE", Toast.LENGTH_SHORT).show();

            Intent intento = new Intent(this, Menu.class);
            startActivity(intento);
        } else Toast.makeText(this,"CORREO INVÁLIDO", Toast.LENGTH_SHORT).show();
    }
}
