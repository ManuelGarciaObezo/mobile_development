package com.example.sportsapps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText email, pass;
    private final String ARCHIVO = "Login";
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private String Sname, Semail, Spass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email = findViewById(R.id.correo);
        pass = findViewById(R.id.pass);

        cargarPrefs();
    }

    public void goMenu(View v){
        if(email.getText().toString().equals(Semail) && pass.getText().toString().equals(Spass)) {
            Toast.makeText(this, "INGRESO CORRECTO", Toast.LENGTH_SHORT).show();
            Intent intento = new Intent(this, Menu.class);
            startActivity(intento);
        } else Toast.makeText(this,"CORREO Y/O CONTRASEÑA INCORRECTOS", Toast.LENGTH_SHORT).show();
    }

    public void goRegister(View v){
        Intent intento = new Intent(this, Register.class);
        startActivity(intento);
    }

    public void cargarPrefs(){
        prefs = getSharedPreferences(ARCHIVO, Context.MODE_PRIVATE);
        editor = prefs.edit();
        Sname = prefs.getString("name","");
        Semail = prefs.getString("email","");
        Spass = prefs.getString("password","");
    }
}
