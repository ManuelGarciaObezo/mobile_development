package com.example.sportsapps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }
    public void goNoticias(View v){
        Intent intento = new Intent(this, Noticias.class);
        startActivity(intento);
    }
    public void goPosiciones(View v){
        Intent intento = new Intent(this, Posiciones.class);
        startActivity(intento);
    }
    public void goResultados(View v){
        Intent intento = new Intent(this, Resultados.class);
        startActivity(intento);
    }

}
